import { EnvService } from './env.service';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  isLoggedIn = false;
  token: any;

  constructor(
    private http: HttpClient,
    private storage: NativeStorage,
    private env: EnvService
  ) { }

  // Login function
  login(username: String, password: String) {
    let url = this.env.API_URL + 'auth/login';
    let data = {
      username: username,
      password: password
    }
    return this.http.post(url, data);
    
    // return this.http.post(url, { username, password });
      // .pipe(
        // tap(token => {
        //   this.storage.setItem('token', token)
        //     .then( () => console.log('Token Stored'),
        //       error => console.error('Error storing item', error)
        //     );
        //   this.token = token;
        //   this.isLoggedIn = true;
        //   return token;
        // }),
      // );
  }

  // Register function
  register(fullName: String, email: String, username: String, password: String) {
    let url = this.env.API_URL + 'auth/register';
    let data = {
      name: fullName,
      email,
      username,
      password
    };
    return this.http.post(url, data);
  }

  // Logout function
  logout() {
  }

  getToken() {
    return this.storage.getItem('token').then(
      data => {
        this.token = data;
        if (this.token != null) {
          this.isLoggedIn = true;
        } else {
          this.isLoggedIn = false;
        }
      },
      error => {
        this.token = null;
        this.isLoggedIn = false;
      }
    );
  }
}
