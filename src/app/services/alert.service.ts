import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(private toastController: ToastController) {}

  createToast(message: string, color: string) {
    return this.toastController.create({
      message: message,
      duration: 2000,
      position: 'top',
      color: color,
    });
  }

  async errorToast(message: string) {
    const toast = await this.createToast(message, "danger");
    toast.present();
  }

  async successToast(message: string) {
    const toast = await this.createToast(message, "success");
    toast.present();
  }

  async warningToast(message: string) {
    const toast = await this.createToast(message, "warning");
    toast.present();
  }

  async infoToast(message: string) {
    const toast = await this.createToast(message, "primary");
    toast.present();
  }
}
